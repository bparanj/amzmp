**Markdown** is great.

```js
var foo = 111;
```

```ruby
class Foo
  def self.greet
    p 'hi'
  end
end
```

Fenced code block:

```
function test() {
  console.log("notice the blank line before this function?");
}
```

# The largest heading (an <h1> tag)
## The second largest heading (an <h2> tag)
…
###### The 6th largest heading (an <h6> tag)
	
	
Blockquotes In the words of Abraham Lincoln:

> Pardon my french

Styling Text

*This text will be italic*
**This text will be bold**


Unordered Lists

* Item
* Item
* Item

- Item
- Item
- Item

Ordered Lists

1. Item 1
2. Item 2
3. Item 3

Nested Lists

1. Item 1
  1. A corollary to the above item.
  2. Yet another point to consider.
2. Item 2
  * A corollary that does not need to be ordered.
    * This is indented four spaces, because it's two spaces further than the item above.
    * You might want to consider making a new list.
3. Item 3

Links

[Visit RubyPlus!](https://www.rubyplus.com)